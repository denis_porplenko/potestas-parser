<?php

class FormIterator
{
    private $law_position = 0;
    private $instance_position = 0;
    private $refereing_form_position = 0;
    private $case_category_position = 0;
    private $law_process_position = 0;

    private $position = [
        'law' => 0,
        'instance' => 0,
        'refereing' => 0,
        'case_category' => 0,
        'law_process' => 0
    ];

    private $law = [
    '2' => 'Автономна Республіка Крим',
    '3' => 'Вінницька область',
    '4' => 'Волинська область',
    '5' => 'Дніпропетровська область',
    '6' => 'Донецька область',
    '7' => 'Житомирська область',
    '8' => 'Закарпатська область',
    '9' => 'Запорізька область',
    '10' => 'Івано-Франківська область',
    '11' => 'Київська область',
    '12' => 'Кіровоградська область',
    '13' => 'Луганська область',
    '14' => 'Львівська область',
    '15' => 'Миколаївська область',
    '16' => 'Одеська область',
    '17' => 'Полтавська область',
    '18' => 'Рівненська область',
    '19' => 'Сумська область',
    '20' => 'Тернопільська область',
    '21' => 'Харківська область',
    '22' => 'Херсонська область',
    '23' => 'Хмельницька область',
    '24' => 'Черкаська область',
    '25' => 'Чернівецька область',
    '26' => 'Чернігівська область',
    '27' => 'м. Київ',
    '28' => 'м. Севастополь',
    '29' => 'ВС Центральний регіон',
    '30' => 'ВС Західний регіон',
    '31' => 'ВС Південний регіон',
    '32' => 'ВС ВМС України',
    ];

    private $instance = ['3' => 'Касаційна'];

    private $refereing_form = [
    '4' => 'Адміністративне',
    '3' => 'Господарське',
    '2' => 'Кримінальне',
    '1' => 'Цивільне',
    '5' => 'Справи про адміністративні правопорушення',
    ];

    private $case_category = [
    '3000' => 'Адміністративні справи',
    '4000' => 'Господарські справи',
    '2000' => 'Кримінальні справи',
    '6258' => 'Невідкладні судові розгляди',
    '5139' => 'Справи про адмінправопорушення',
    '1000' => 'Цивільні справи',
    ];

    private $law_process = [
    '1' => 'Фізична особа',
    '2' => 'Державний орган, підприємство, установа, організація',
    '3' => 'Юридична особа',
    ];

    public function __construct()
    {
        $this->position['law'] = array_shift($this->law);
        $this->position['instance'] = array_shift($this->instance);
        $this->position['refereing_form'] = array_shift($this->refereing_form);
        $this->position['case_category'] = array_shift($this->case_category);
        $this->position['law_process'] = array_shift($this->law_process);
    }

    function rewind($filter_name)
    {
        if ($this->validator($filter_name)) {
            $this->$filter_name = array_shift($this->$filter_name);
        }
    }

    function current($filter_name)
    {
        return $this->position[$filter_name];
    }

    function next($filter_name)
    {
        if ($this->validator($filter_name)) {
            next($this->$filter_name);
        }
    }

    function validator($filter_name)
    {
        $object_vars = get_object_vars($this);
        if (in_array($filter_name, $object_vars)) {
            return true;
        }
    }

}