<?php

require '../vendor/autoload.php';

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;

function getNodeText($node)
{
    if($node instanceof \DOMElement) {
        return trim($node->nodeValue);
    }

    return null;
}

$start_date = Carbon::create('2000', '01', '01');

$law = [
    '2' => 'Автономна Республіка Крим',
    '3' => 'Вінницька область',
    '4' => 'Волинська область',
    '5' => 'Дніпропетровська область',
    '6' => 'Донецька область',
    '7' => 'Житомирська область',
    '8' => 'Закарпатська область',
    '9' => 'Запорізька область',
    '10' => 'Івано-Франківська область',
    '11' => 'Київська область',
    '12' => 'Кіровоградська область',
    '13' => 'Луганська область',
    '14' => 'Львівська область',
    '15' => 'Миколаївська область',
    '16' => 'Одеська область',
    '17' => 'Полтавська область',
    '18' => 'Рівненська область',
    '19' => 'Сумська область',
    '20' => 'Тернопільська область',
    '21' => 'Харківська область',
    '22' => 'Херсонська область',
    '23' => 'Хмельницька область',
    '24' => 'Черкаська область',
    '25' => 'Чернівецька область',
    '26' => 'Чернігівська область',
    '27' => 'м. Київ',
    '28' => 'м. Севастополь',
    '29' => 'ВС Центральний регіон',
    '30' => 'ВС Західний регіон',
    '31' => 'ВС Південний регіон',
    '32' => 'ВС ВМС України',
];

$instance = ['3' => 'Касаційна'];

$refereing_form = [
    '4' => 'Адміністративне',
    '3' => 'Господарське',
    '2' => 'Кримінальне',
    '1' => 'Цивільне',
    '5' => 'Справи про адміністративні правопорушення',
];

$case_category = [
    '3000' => 'Адміністративні справи',
    '4000' => 'Господарські справи',
    '2000' => 'Кримінальні справи',
    '6258' => 'Невідкладні судові розгляди',
    '5139' => 'Справи про адмінправопорушення',
    '1000' => 'Цивільні справи',
];

$law_process = [
    '1' => 'Фізична особа',
    '2' => 'Державний орган, підприємство, установа, організація',
    '3' => 'Юридична особа',
];

$client = new Client();
$crawler = $client->request('GET', 'http://www.reyestr.court.gov.ua/');
$form = $crawler->selectButton('Пошук')->form();

$crawler = $client->submit(
    $form,
    [
        'RegDateBegin' => '01.01.2013',
        'RegDateEnd' => '20.01.2013',
        'Sort' => '0',
        'PagingInfo.ItemsPerPage' => 25,
        'Liga' => true
    ]
);
print($crawler->html());
$count = $crawler->filter('#divTopNavigate');
print($count->html());

die();

$trs = $crawler->filter('div#divresult tr');

$trs->each(function ($tr, $key) {
    if($key > 0) {
        /**
         * @var Crawler $tr
         * */
        /**
         * @var \DOMElement $vr_type
         * */
        /**
         * @var \DOMElement $a
         * */

        $a = $tr->filter('a.doc_text2')->first()->getNode(0);
        $href_text = getNodeText($a);
        $href = $a->getAttribute('href');
        $description = new Client();
        $description_query = $description->request('GET', 'http://www.reyestr.court.gov.ua' . $href);
        $description_page = $description_query->filter('#txtdepository');
        $html = '';
        $text = '';
        $description_page->each(function(Crawler $crawler) {
            global $html, $text;
            $text = $crawler->text();
            $html = $crawler->html();
            file_put_contents('description.html', $html);
        });
        $vr_type = getNodeText($tr->filter('td.VRType')->first()->getNode(0));
        $reg_data = getNodeText($tr->filter('td.RegDate')->first()->getNode(0));
        $law_data = getNodeText($tr->filter('td.LawDate')->first()->getNode(0));
        $cs_type = getNodeText($tr->filter('td.CSType')->first()->getNode(0));
        $case_number = getNodeText($tr->filter('td.CaseNumber')->first()->getNode(0));
        $court_name = getNodeText($tr->filter('td.CourtName')->first()->getNode(0));
        $court_code = getNodeText($tr->filter('td.CourtCode')->first()->getNode(0));
        $chairmen_name = getNodeText($tr->filter('td.ChairmenName')->first()->getNode(0));
        sleep(rand(3,5));
    }

});
